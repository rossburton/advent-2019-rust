use num_enum::TryFromPrimitive;
use std::convert::TryFrom;
use std::fs::File;
use std::io::{self, BufRead};

#[derive(PartialEq,Debug,TryFromPrimitive)]
#[repr(i32)]
pub enum Opcode {
    OpAdd = 1,
    OpMultiply = 2,
    OpHalt = 99
}

pub fn decode_opcode(value: i32) -> Opcode {
    let s = format!("{:05}", value);
    // TODO: don't die here, but return result
    let opcode = Opcode::try_from(b[4..5]).expect("Unknown opcode");
    return opcode;
}

pub fn run(memory: &mut [i32]) {
    let mut ip: usize = 0;
    while ip < memory.len() {
        let opcode = Opcode::try_from(memory[ip]).expect("Unknown opcode");
        ip += 1;
        match opcode {
            Opcode::OpAdd => {
                let a = memory[ip] as usize;
                ip += 1;
                let b = memory[ip] as usize;
                ip += 1;
                let target = memory[ip] as usize;
                ip += 1;
                memory[target] = memory[a] + memory[b];
            },
            Opcode::OpMultiply => {
                let a = memory[ip] as usize;
                ip += 1;
                let b = memory[ip] as usize;
                ip += 1;
                let target = memory[ip] as usize;
                ip += 1;
                memory[target] = memory[a] * memory[b];
            },
            Opcode::OpHalt => {
                return;
            }
        }
    }
}

pub fn parse_from_file(filename: &str) -> Vec<i32> {
    // Open @filename or die
    let file = File::open(filename).expect("Cannot find file");
    // String buffer
    let mut line = String::new();
    // Read a line or die
    io::BufReader::new(file).read_line(&mut line).expect("Cannot read line");
    // Split the line on commas, parse each string and unwrap the error, then
    // collect into a vector.
    let program: Vec<i32> = line.split(',').map(|s| s.parse().unwrap()).collect();
    return program;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_day_2_example() {
        let mut memory = vec![1,9,10,3,2,3,11,0,99,30,40,50];
        run(memory.as_mut_slice());
        let expected = vec![3500,9,10,70,2,3,11,0,99,30,40,50];
        assert_eq!(memory[..], expected[..], "\nExpected\n{:?}\nfound\n{:?}", &expected[..], &memory[..]);
    }

    #[test]
    fn test_decode_opcode() {
        assert_eq!(decode_opcode(1), Opcode::OpAdd);
        assert_eq!(decode_opcode(2), Opcode::OpMultiply);
        assert_eq!(decode_opcode(99), Opcode::OpHalt);
        // TODO when decode returns a result test invalid opcodes
    }
}
