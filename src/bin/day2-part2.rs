extern crate intcode;

fn main() {
    let program = intcode::parse_from_file("day2-input");
    for noun in 0..100 {
        for verb in 0..100 {
            let mut memory = program.clone();
            memory[1] = noun;
            memory[2] = verb;
            intcode::run(memory.as_mut_slice());
            if memory[0] == 19690720 {
                println!("Result {} from noun {} verb {} magic number {}",
                    memory[0], noun, verb, 100 * noun + verb);
                return;
            }
        }
    }
    println!("Count not determine noun and verb");
}
