extern crate intcode;

fn main() {
    let mut program = intcode::parse_from_file("day2-input");
    program[1] = 12;
    program[2] = 2;
    intcode::run(program.as_mut_slice());
    println!("Result {}", program[0]);
}
