use std::fs::File;
use std::io::{self, BufRead};

fn main() {
    let mut total = 0;
    let file = File::open("day1-input").expect("Cannot find file");
    let lines = io::BufReader::new(file).lines().map(|l| l.unwrap());

    for line in lines {
        let mass: i32 = line.trim().parse().expect("Cannot parse mass");
        let fuel = fuel_really_needed(mass);
        total += fuel;
        println!("Mass {} -> fuel {}", mass, fuel);
    }
    println!("Total {}", total);
}

pub fn fuel_needed(mass: i32) -> i32 {
    return mass / 3 - 2;
}

pub fn fuel_really_needed(mass: i32) -> i32 {
    let mut mass = mass;
    let mut total = 0;
    while mass > 0 {
        mass = mass / 3 - 2;
        if mass > 0 {
            total += mass;
        }
    }
    return total;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_basic_fuel() {
        assert_eq!(fuel_needed(12), 2);
        assert_eq!(fuel_needed(14), 2);
        assert_eq!(fuel_needed(1969), 654);
        assert_eq!(fuel_needed(100756), 33583);
    }

    #[test]
    fn test_real_fuel() {
        assert_eq!(fuel_really_needed(12), 2);
        assert_eq!(fuel_really_needed(14), 2);
        assert_eq!(fuel_really_needed(1969), 966);
        assert_eq!(fuel_really_needed(100756), 50346);
    }
}